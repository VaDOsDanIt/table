const headerTabsButtons = document.querySelectorAll(".header__tabs .tabs__button");
const mainTabsBody = document.querySelectorAll(".main");

headerTabsButtons.forEach(button => {
    button.addEventListener('click', () => {
        mainTabsBody.forEach(body => {
            if (button.getAttribute("data-id") === body.getAttribute("data-id")) {
                hideAllElements(mainTabsBody);
                body.classList.remove('hide');
            }
        })
    });
});


const hideAllElements = (elementsArr) => {
    elementsArr.forEach(item => {
        item.classList.add('hide');
    })
};


const leaveBlockButton = document.querySelectorAll(".container__leave-block-button");

leaveBlockButton.forEach(item => {
    item.addEventListener('click', () => {
        item.previousElementSibling.classList.toggle('hideStat');
        item.parentElement.classList.toggle('hideStatParent');
        item.parentElement.parentElement.classList.toggle('slideTable');
        item.classList.toggle('changeArrow');
    })
});

const tableHeaderElement = document.querySelectorAll(".table__header-column-element");
const tableColsElement = document.querySelectorAll('.table__body-column-element,.footer__column-element');

const slide = (tableHeaderElement, tableColsElement, selectedElement, idNextSlide) => {
    tableHeaderElement.forEach(item => {
        tableColsElement.forEach(el => {
            if (parseInt(item.getAttribute('data-id'), 10) === selectedElement && parseInt(el.getAttribute('data-id'), 10) === selectedElement) {
                item.classList.remove('for-mobile');
                el.classList.remove('for-mobile');
            } else if (parseInt(item.getAttribute('data-id'), 10) === idNextSlide && parseInt(el.getAttribute('data-id'), 10) === idNextSlide) {
                item.classList.remove('for-mobile');
                el.classList.remove('for-mobile');
                item.classList.add('for-mobile');
                el.classList.add('for-mobile');
            }
        })
    });
};


const nextSlide = () => {
    const target = event.target;
    const countElements = target.parentElement.parentElement.childElementCount - 2;
    const maxElement = parseInt(target.parentElement.parentElement.lastElementChild.getAttribute('data-id'), 10);
    const minElement = maxElement - countElements;

    setTimeout((e) => {
        const selectedElement = parseInt(target.parentElement.getAttribute("data-id"), 10);
        const idNextSlide = selectedElement + 1;
        if ((selectedElement >= minElement && selectedElement < maxElement)) {
            slide(tableHeaderElement, tableColsElement, selectedElement, idNextSlide, target);
        }
    }, 0)
};

const prevSlide = () => {
    const target = event.target;
    const countElements = target.parentElement.parentElement.childElementCount - 2;
    const maxElement = parseInt(target.parentElement.parentElement.lastElementChild.getAttribute('data-id'), 10);
    const minElement = maxElement - countElements;

    setTimeout(() => {
        const selectedElement = parseInt(target.parentElement.getAttribute("data-id"), 10);
        const idPrevSlide = selectedElement - 1;
        if (idPrevSlide >= minElement && idPrevSlide < maxElement) {
            slide(tableHeaderElement, tableColsElement, selectedElement, idPrevSlide);
        }
    }, 0)
};



